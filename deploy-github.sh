#!/bin/bash
# GitHub deploy script
# author: Ondrej Sika

GITHUB_REPO="git@github.com:zoorilla/zoorilla.github.io.git"

sh build.sh

cd build

gittmp="/tmp/git$RANDOM$RANDOM"

git --git-dir=$gittmp --work-tree="." init
git --git-dir=$gittmp --work-tree="." remote add origin $GITHUB_REPO
git --git-dir=$gittmp --work-tree="." add . -f
git --git-dir=$gittmp --work-tree="." commit -a -m "Build & deploy"
git --git-dir=$gittmp push origin master:gh-pages --force

rm $gittmp -rf
