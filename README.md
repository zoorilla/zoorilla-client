Zoorilla client
===============

### Maintainer
* Ondrej Sika, <http://ondrejsika.com>, <ondrej@ondrejsika.com>

### Authors
* Lukas Rampa
* Ondrej Sika

### Source
* GitHub <https://github.com/zoorilla/zoorilla-client>


Documentation
-------------

Running on <http://zoorilla.github.io>


### Requirements

* coffee script (node.js) - `npm install -g coffee-script`



### Development

Run development server on port 9999 `sh serve.sh`

Build site `sh build.sh`.

Deploy to GitHub pages `sh deploy-github.sh`
