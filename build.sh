#!/bin/bash

rm build -rf
mkdir build

cp index.html build
cp static build -r

coffee --bare --compile build/static

find ./build -name "*.coffee" -exec rm {} \;
